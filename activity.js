/*=========================================================================*/
db.room.insertOne({
	name: "single",
	accomodates: "2",
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_avaliable: 10,
	isAvaliable: false
});

/*=========================================================================*/

db.room.insertMany([
	{
		name: "double",
		accomodates: "3",
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_avaliable: 5,
		isAvaliable: false
	},
	{
		name: "queen",
		accomodates: "4",
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_avaliable: 15,
		isAvaliable: false
	}
	]);

/*=========================================================================*/

db.room.find({
	name: "double"
});

/*=========================================================================*/

db.room.updateOne(
	{
		name: "queen"
	},
	{ $set:
	{
		rooms_avaliable: 0
	}

	}
);

/*=========================================================================*/

db.room.deleteMany ({
	rooms_avaliable: 0
});